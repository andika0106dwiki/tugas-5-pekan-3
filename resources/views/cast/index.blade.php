@extends('adminlte.master')
@section('title','Cast')
@section('content')
<div class="container">
    <h4 style="padding:10px">Table Cast</h4>
    <div class="card">
        <div class="card-header">
            <a href="{{ route('create.cast') }}" class="btn btn-success">Tambah Cast</a>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Nama</th>
                        <th>Umur</th>
                        <th>Bio</th>
                        <th style="width: 280px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($casts as $cast => $value)
                    <tr>
                        <td>{{ $cast + 1 }}</td>
                        <td>{{ $value->nama }}</td>
                        <td>{{ $value->umur }}</td>
                        <td>{{ $value->bio }}</td>
                        <td>

                            <form action="{{ route('cast.delete', $value->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="{{ route('cast.show', $value->id) }}" class="btn btn-info">Show</a>
                                <a href="{{ route('cast.edit', $value->id) }}" class="btn btn-primary">Edit</a>
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                    @empty
                    {{-- <div class="alert alert-danger" role="alert">
                        Tidak ada data !
                    </div> --}}
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src=" {{ asset('/adminlte/plugins/datatables/jquery.dataTables.js') }} "></script>
<script src=" {{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }} "></script>
<script>
    $(function () {
    $("#example1").DataTable();
    });
</script>
@endpush
