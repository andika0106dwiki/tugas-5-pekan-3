@extends('adminlte.master')
@section('title','Cast Show')
@section('content')
<div class="container">
    <h4 style="padding:10px">Info Cast</h4>
</div>
<div class="card mx-3">
    <div class="card-header text-white bg-secondary">
        <div class="card-title">
            <a class="btn btn-info" href="{{ route('cast') }}">Kembali</a>
        </div>
    </div>
    <div class="card-header ">
        <h2 class="text-center">{{ $cast->nama }}</h2>
    </div>
    <div class="card-body">

        <strong>Umur : </strong>
        <p>{{ $cast->umur }}</p>
        <p>Bio :</p>
        <strong>{{ $cast->bio }}</strong>
    </div>
</div>

@endsection
