<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/table', function () {
    return view('table');
})->name('table');

Route::get('/data-tables', function () {
    return view('data-tables');
})->name('data-tables');

Route::get('cast', 'CastController@index')->name('cast');
Route::get('cast/create', 'CastController@create')->name('create.cast');
Route::post('cast', 'CastController@store')->name('cast.store');
Route::get('cast/{cast_id}', 'CastController@show')->name('cast.show');
Route::get('cast/{cast_id}/edit', 'CastController@edit')->name('cast.edit');
Route::put('cast/{cast_id}', 'CastController@update')->name('cast.update');
Route::delete('cast/{cast_id}', 'CastController@destroy')->name('cast.delete');
